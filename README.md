# cute-dnd-dice

[![Build](https://gitlab.com/Douman/cute-dnd-dice/badges/master/pipeline.svg)](https://gitlab.com/Douman/cute-dnd-dice/pipelines)
[![Crates.io](https://img.shields.io/crates/v/cute-dnd-dice.svg)](https://crates.io/crates/cute-dnd-dice)
[![Documentation](https://docs.rs/cute-dnd-dice/badge.svg)](https://docs.rs/crate/cute-dnd-dice/)

Simple library to roll dices

## Random source

Relies on `getrandom` by default

## Usage

```rust
use cute_dnd_dice::Roll;

fn main() {
    let roll = Roll::from_str("2d20+10").expect("To parse roll");
    println!("I roll {}", roll.roll());
}
```
