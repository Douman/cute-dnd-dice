use core::mem;

use getrandom::getrandom;

pub fn random() -> u64 {
    let mut result = [0; mem::size_of::<u64>()];
    if let Err(error) = getrandom(&mut result) {
        panic!("OS Random is not available: {}", error);
    }
    u64::from_ne_bytes(result)
}
