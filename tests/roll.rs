use cute_dnd_dice::{Modifier, ParseError, Roll};

#[inline(always)]
fn faces(faces: u16) -> core::num::NonZeroU16 {
    core::num::NonZeroU16::new(faces).unwrap()
}

#[test]
fn fmt_roll() {
    let mut roll = Roll::new(2, faces(20), Modifier::Plus(0));
    let expected_fmt = "2d20";
    assert_eq!(format!("{}", roll), expected_fmt);

    roll.extra = roll.extra + 5;
    let expected_fmt = "2d20+5";
    assert_eq!(format!("{}", roll), expected_fmt);

    roll.extra = Modifier::Minus(5);
    let expected_fmt = "2d20-5";
    assert_eq!(format!("{}", roll), expected_fmt);
}

#[test]
fn try_roll() {
    let mut roll = Roll::new(1, faces(20), Modifier::Plus(0));
    let min = roll.min();
    assert_eq!(min, 1);
    let max = roll.max();
    assert_eq!(max, 20);
    let mut vals = std::collections::BTreeMap::new();

    for _ in 0..9000 {
        let roll = roll.roll();
        assert!(roll >= min);
        assert!(roll <= max);

        if let Some(val) = vals.get_mut(&roll) {
            *val += 1;
        } else {
            vals.insert(roll, 1usize);
        }
    }

    //range is inclusive therefore +1
    if vals.len() != max as usize - min as usize + 1 {
        for (key, value) in vals.iter() {
            println!("{}: {}", key, value);
        }
        panic!("Failed to generated good enough distribution");
    }
    vals.clear();

    roll = Roll::new(2, faces(20), Modifier::Plus(4));

    let min = roll.min();
    assert_eq!(min, 6);
    let max = roll.max();
    assert_eq!(max, 44);

    for _ in 0..9000 {
        let roll = roll.roll();
        assert!(roll >= min);
        assert!(roll <= max);

        if let Some(val) = vals.get_mut(&roll) {
            *val += 1;
        } else {
            vals.insert(roll, 1usize);
        }
    }

    //range is inclusive therefore +1
    if vals.len() != max as usize - min as usize + 1 {
        for (key, value) in vals.iter() {
            println!("{}: {}", key, value);
        }
        panic!("Failed to generated good enough distribution");
    }
    vals.clear();

    roll.extra = Modifier::Minus(20);
    let min = roll.min();
    assert_eq!(min, 0);
    let max = roll.max();
    assert_eq!(max, 20);

    for _ in 0..9000 {
        let roll = roll.roll();
        assert!(roll >= min);
        assert!(roll <= max);

        if let Some(val) = vals.get_mut(&roll) {
            *val += 1;
        } else {
            vals.insert(roll, 1usize);
        }
    }

    //range is inclusive therefore +1
    if vals.len() != max as usize - min as usize + 1 {
        for (key, value) in vals.iter() {
            println!("{}: {}", key, value);
        }
        panic!("Failed to generated good enough distribution")
    }
}

#[test]
fn should_fail_parse_roll() {
    let result = Roll::from_str("0d20").expect_err("to error");
    assert_eq!(result, ParseError::InvalidNum);

    let result = Roll::from_str("1d0").expect_err("to error");
    assert_eq!(result, ParseError::InvalidFaces);

    let result = Roll::from_str("2d20+").expect_err("to error");
    assert_eq!(result, ParseError::MissingModifierValue);

    let result = Roll::from_str("2d20-").expect_err("to error");
    assert_eq!(result, ParseError::MissingModifierValue);

    let result = Roll::from_str("2d20+   ").expect_err("to error");
    assert_eq!(result, ParseError::MissingModifierValue);

    let result = Roll::from_str("2d20-   ").expect_err("to error");
    assert_eq!(result, ParseError::MissingModifierValue);

    let result = Roll::from_str("2d20+ text").expect_err("to error");
    assert_eq!(result, ParseError::InvalidExtra);

    let result = Roll::from_str("2d20+-1").expect_err("to error");
    assert_eq!(result, ParseError::InvalidExtra);

    let result = Roll::from_str("2d20--1").expect_err("to error");
    assert_eq!(result, ParseError::InvalidExtra);

    let result = Roll::from_str("2dtext+20").expect_err("to error");
    assert_eq!(result, ParseError::InvalidFaces);

    let result = Roll::from_str("2dtext").expect_err("to error");
    assert_eq!(result, ParseError::InvalidFaces);

    let result = Roll::from_str("smold20").expect_err("to error");
    assert_eq!(result, ParseError::InvalidNum);

    let result = Roll::from_str("dd20").expect_err("to error");
    assert_eq!(result, ParseError::InvalidFaces);

    let result = Roll::from_str("20").expect_err("to error");
    assert_eq!(result, ParseError::MissingD);

    let result = Roll::from_str("1d").expect_err("to error");
    assert_eq!(result, ParseError::MissingFaces);

    let result = Roll::from_str("d").expect_err("to error");
    assert_eq!(result, ParseError::MissingFaces);
}

#[test]
fn should_parse_roll() {
    let result = Roll::from_str("2d20+10").expect("to parse");
    assert_eq!(result.num, 2);
    assert_eq!(result.faces.get(), 20);
    assert_eq!(result.extra, Modifier::Plus(10));

    let result = Roll::from_str("2 d 20 + 10").expect("to parse");
    assert_eq!(result.num, 2);
    assert_eq!(result.faces.get(), 20);
    assert_eq!(result.extra, Modifier::Plus(10));

    let result = Roll::from_str("2 d 20").expect("to parse");
    assert_eq!(result.num, 2);
    assert_eq!(result.faces.get(), 20);
    assert_eq!(result.extra, Modifier::Plus(0));

    let result = Roll::from_str("d1").expect("to parse");
    assert_eq!(result.num, 1);
    assert_eq!(result.faces.get(), 1);
    assert_eq!(result.extra, Modifier::Plus(0));

    let result = Roll::from_str("4d1").expect("to parse");
    assert_eq!(result.num, 4);
    assert_eq!(result.faces.get(), 1);
    assert_eq!(result.extra, Modifier::Plus(0));

    let result = Roll::from_str("4D1").expect("to parse");
    assert_eq!(result.num, 4);
    assert_eq!(result.faces.get(), 1);
    assert_eq!(result.extra, Modifier::Plus(0));

    let result = Roll::from_str("2d20-9").expect("to parse");
    assert_eq!(result.num, 2);
    assert_eq!(result.faces.get(), 20);
    assert_eq!(result.extra, Modifier::Minus(9));
}

#[test]
fn test_modifier_ops() {
    let mut modifier = Modifier::Plus(0) + 1;
    assert_eq!(modifier, Modifier::Plus(1));

    modifier -= 20;
    assert_eq!(modifier, Modifier::Minus(19));

    let modifier = modifier + 19;
    assert_eq!(modifier, Modifier::Plus(0));

    let modifier = modifier + 19;
    assert_eq!(modifier, Modifier::Plus(19));

    let modifier = modifier - 19;
    assert_eq!(modifier, Modifier::Plus(0));
}
